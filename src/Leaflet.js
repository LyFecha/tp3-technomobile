import React from "react";

function Leaflet({ data, filter }) {
  const liste = data.Countries.filter((country) =>
    country.Country.toLowerCase().includes(filter.toLowerCase())
  ).map((country) => (
    <tr key={country.Country}>
      {Object.keys(country).map((key) => (
        <td key={"_" + key}>{country[key]}</td>
      ))}
    </tr>
  ));

  return <div className="shadow p-3 mb-5 bg-white rounded"></div>;
}

export default Leaflet;
