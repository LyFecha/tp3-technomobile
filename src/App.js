import React from "react";
import Liste from "./Liste";
import Leaflet from "./Leaflet";
import fakedata from "./fakedata.json";
import "./App.css";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      data: fakedata,
      filter: "",
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    fetch("https://api.covid19api.com/summary")
      .then((data) => data.json())
      .then((json) => this.setState({ data: json }));
  }

  handleChange({ target }) {
    this.setState({ filter: target.value });
  }

  render() {
    return (
      <div className="App">
        <input placeholder="Filtre" onChange={this.handleChange}></input>
        <Liste data={this.state.data} filter={this.state.filter}></Liste>
        <Leaflet data={this.state.data} filter={this.state.filter}></Leaflet>
      </div>
    );
  }
}

export default App;
