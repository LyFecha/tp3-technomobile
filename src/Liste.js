import React from "react";

function Liste({ data, filter }) {
  const liste = data.Countries.filter((country) =>
    country.Country.toLowerCase().includes(filter.toLowerCase())
  ).map((country) => (
    <tr key={country.Country}>
      {Object.keys(country).map((key) => (
        <td key={"_" + key}>{country[key]}</td>
      ))}
    </tr>
  ));

  return (
    <div className="shadow p-3 mb-5 bg-white rounded">
      <table className="table table-striped table-bordered">
        <thead className="thead-light">
          <tr>
            {Object.keys(data.Countries[0]).map((key) => (
              <th key={key}>{key}</th>
            ))}
          </tr>
        </thead>
        <tbody>{liste}</tbody>
      </table>
    </div>
  );
}

export default Liste;

/*
            <th>Token</th>
            <th>Score</th>
            <th>Difficulty</th>
            <th>Category</th>


          {data.Countries}
          <tr>
            <td>
              <input
                name="token"
                value={this.state.token}
                onChange={this.handleChange}
                placeholder="Token"
              />
            </td>
            <td>
              <input
                name="score"
                value={this.state.score}
                onChange={this.handleChange}
                placeholder="Score"
              />
            </td>
            <td>
              <select
                name="difficulty"
                value={this.state.difficulty}
                onChange={this.handleChange}
              >
                {DIFFICULTY.map((elem) => (
                  <option key={elem[0]} value={elem[1]}>
                    {elem[1]}
                  </option>
                ))}
              </select>
            </td>
            <td>
              <select
                name="category"
                value={this.state.category}
                onChange={this.handleChange}
              >
                {CATEGORIES.map((elem) => (
                  <option key={elem[0]} value={elem[1]}>
                    {elem[1]}
                  </option>
                ))}
              </select>
            </td>
          </tr>

          
      <button name="add" onClick={this.handleClick}>
        Add
      </button>
      <button name="remove" onClick={this.handleClick}>
        Remove :
      </button>
      <input
        name="removeLine"
        value={this.state.removeLine}
        onChange={this.handleChange}
        placeholder="Line to remove"
      />
*/
